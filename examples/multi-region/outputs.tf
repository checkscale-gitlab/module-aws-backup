####
# Vault
####

output "vault_id" {
  value = module.multi_region1.vault_id
}

output "vault_arn" {
  value = module.multi_region1.vault_arn
}

output "vault_recovery_points" {
  value = module.multi_region1.vault_recovery_points
}

output "vault_kms_key_arn" {
  value = module.multi_region1.vault_kms_key_arn
}

output "vault_kms_key_id" {
  value = module.multi_region1.vault_kms_key_id
}

output "vault_kms_key_alias_arn" {
  value = module.multi_region1.vault_kms_key_alias_arn
}

output "vault_kms_key_replica_arn" {
  value = module.multi_region1.vault_kms_key_replica_arn
}

output "vault_kms_key_replica_id" {
  value = module.multi_region1.vault_kms_key_replica_id
}

####
# Plan
####

output "plan_arns" {
  value = module.multi_region1.plan_arns
}

output "plan_versions" {
  value = module.multi_region1.plan_versions
}

output "selection_iam_role_arn" {
  value = module.multi_region1.selection_iam_role_arn
}

output "selection_iam_role_name" {
  value = module.multi_region1.selection_iam_role_name
}

output "selection_iam_role_unique_id" {
  value = module.multi_region1.selection_iam_role_unique_id
}

####
# Selection
####

output "selection_tag_id" {
  value = module.multi_region1.selection_tag_id
}

output "selection_resources_ids" {
  value = module.multi_region1.selection_resources_ids
}
