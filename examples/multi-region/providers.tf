provider "aws" {
  region     = "us-east-1"
  access_key = var.aws_access_key
  secret_key = var.aws_secret_key
  profile    = var.aws_profile

  assume_role {
    role_arn = var.aws_assume_role
  }
}

provider "aws" {
  region     = "us-east-2"
  access_key = var.aws_access_key
  secret_key = var.aws_secret_key
  profile    = var.aws_profile
  alias      = "us2"

  assume_role {
    role_arn = var.aws_assume_role
  }
}
