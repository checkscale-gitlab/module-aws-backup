terraform {
  required_version = ">= 1"

  experiments = [module_variable_optional_attrs]

  required_providers {
    aws = {
      source                = "hashicorp/aws"
      version               = ">= 3.73"
      configuration_aliases = [aws.us2]
    }
  }
}
